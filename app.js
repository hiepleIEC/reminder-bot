const cron = require('node-cron')
const { myFunction } = require('.')

// */10 * * * * *  run every 10 seconds fo test
cron.schedule('*/3 * * * *', () => {
  console.log('Running task every 3 minutes', new Date())
  myFunction()
})
