require('dotenv').config()
const moment = require('moment-timezone')
const { google } = require('googleapis')
const fetch = require('node-fetch')
const { getAuth } = require('./google')
const DISCORD_URL = process.env.DISCORD_URL || 'https://discord.com/api/v9'
const SHEET_ID = process.env.SHEET_ID
const TYPE_REMAINDER = {
  ONCE: '1',
  DAILY: '2',
  WEEKLY: '3',
  MONTHLY: '4',
  YEARLY: '5',
}
const toTimeZone = (zone) => {
  const format = 'MMMM DD YYYY h:mm:ss a'
  return moment(new Date()).tz(zone).format(format)
}
exports.myFunction = async () => {
  try {
    const { sheets, values } = await getDataFromSheet(SHEET_ID, 'List')
    for (let i = 1; i < values.length; i++) {
      const type = values[i][6]
      const interval = values[i][10]
      const row = i + 1
      if (values[i].length > 0) {
        if (interval) {
          await processMessageFollowInterval(values[i], sheets, row, interval)
        } else {
          await processMessageFollowType(values[i], sheets, row, type)
        }
      }
    }
  } catch (error) {
    console.log(error)
  }
}

const processMessageFollowInterval = async (
  [userID, messageID, channelID, time, , , , content, customTag, expires],
  sheets,
  row,
  interval
) => {
  const nowWithTZ = toTimeZone('Asia/Ho_Chi_Minh')
  if (expires) {
    const expiresFormat = moment(new Date(expires)).format(
      'MMMM DD YYYY h:mm:ss a'
    )
    if (new Date(expiresFormat) < new Date(nowWithTZ))
      return deleteRow(sheets, SHEET_ID, row)
  }
  // check time
  const oldTime = moment(new Date(time)).format('MMMM DD YYYY h:mm:ss a')
  if (new Date(oldTime) < new Date(nowWithTZ)) {
    await sendMessageToDiscord(messageID, channelID, userID, content, customTag)
    // check interval
    const newTime = new Date(new Date(oldTime).getTime() + interval * 1000)
    const rowValueUpdate = [userID, messageID, channelID]
    await updateRowSheet(sheets, row, [
      ...rowValueUpdate,
      moment(newTime).format('MMMM DD YYYY h:mm:ss a'),
    ])
  }
}
const processMessageFollowType = async (
  [userID, messageID, channelID, time, , , , content, customTag, expires],
  sheets,
  row,
  type
) => {
  try {
    const nowWithTZ = toTimeZone('Asia/Ho_Chi_Minh')
    // check expires
    if (expires) {
      const expiresFormat = moment(new Date(expires)).format(
        'MMMM DD YYYY h:mm:ss a'
      )
      if (new Date(expiresFormat) < new Date(nowWithTZ))
        return deleteRow(sheets, SHEET_ID, row)
    }

    const oldTime = moment(new Date(time)).format('MMMM DD YYYY h:mm:ss a')
    const oldTimeObj = new Date(oldTime)
    const rowValueUpdate = [userID, messageID, channelID]
    switch (type) {
      case TYPE_REMAINDER.ONCE: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // delete row in sheet
          await deleteRow(sheets, SHEET_ID, row)
        }
        break
      }
      case TYPE_REMAINDER.DAILY: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          // send message to discord;
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // update row in sheet
          const newTime = new Date(oldTimeObj.setDate(oldTimeObj.getDate() + 1))
          await updateRowSheet(sheets, row, [
            ...rowValueUpdate,
            moment(newTime).format('MMMM DD YYYY h:mm:ss a'),
          ])
        }
        break
      }
      case TYPE_REMAINDER.WEEKLY: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          // send message to discord;
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // update row in sheet
          const newTime = new Date(oldTimeObj.setDate(oldTimeObj.getDate() + 7))
          await updateRowSheet(sheets, row, [
            ...rowValueUpdate,
            moment(newTime).format('MMMM DD YYYY h:mm:ss a'),
          ])
        }
        break
      }
      case TYPE_REMAINDER.MONTHLY: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          // send message to discord;
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // update row in sheet
          const newTime = new Date(
            oldTimeObj.setMonth(oldTimeObj.getMonth() + 1)
          )
          await updateRowSheet(sheets, row, [
            ...rowValueUpdate,
            moment(newTime).format('MMMM DD YYYY h:mm:ss a'),
          ])
        }
        break
      }
      case TYPE_REMAINDER.YEARLY: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          // send message to discord;
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // update row in sheet
          const newTime = new Date(
            oldTimeObj.setFullYear(oldTimeObj.getFullYear() + 1)
          )
          await updateRowSheet(sheets, row, [
            ...rowValueUpdate,
            moment(newTime).format('MMMM DD YYYY h:mm:ss a'),
          ])
        }
        break
      }
      default: {
        if (new Date(oldTime) < new Date(nowWithTZ)) {
          // send message to discord;
          await sendMessageToDiscord(
            messageID,
            channelID,
            userID,
            content,
            customTag
          )
          // delete row in sheet
          await deleteRow(sheets, SHEET_ID, row)
        }
        break
      }
    }
  } catch (error) {
    console.log(error)
  }
}

const getAuthGoogle = async () => {
  const oAuth2Client = await google.auth.getClient({
    scopes: [
      'https://www.googleapis.com/auth/spreadsheets',
      'https://www.googleapis.com/auth/devstorage.read_only',
    ],
  })
  return google.sheets({ version: 'v4', oAuth2Client })
}

const getSheets = () => {
  const auth = getAuth()
  return google.sheets({ version: 'v4', auth })
}

const getDataFromSheet = async (sheetId, sheetName) => {
  const sheets = getSheets()
  const {
    data: { values },
  } = await sheets.spreadsheets.values.get({
    spreadsheetId: sheetId,
    range: `${sheetName}!A:K`,
  })
  return { sheets, values }
}

const updateRowSheet = (sheets, row, rowValue) =>
  sheets.spreadsheets.values.update({
    spreadsheetId: SHEET_ID,
    range: `List!A${row}:D${row}`,
    valueInputOption: 'USER_ENTERED',
    resource: {
      values: [rowValue],
    },
  })

const deleteRow = (sheets, sheetId, row) => {
  const batchUpdateRequest = {
    requests: [
      {
        deleteDimension: {
          range: {
            sheetId: 0,
            dimension: 'ROWS',
            startIndex: parseInt(row - 1),
            endIndex: parseInt(row),
          },
        },
      },
    ],
  }
  return sheets.spreadsheets.batchUpdate({
    spreadsheetId: sheetId,
    resource: batchUpdateRequest,
  })
}

const sendMessageToDiscord = async (
  messageID,
  channelID,
  userID,
  content,
  customTag
) => {
  const payload = { content: `${content} <@${userID}>` }
  if (messageID) payload.message_reference = { message_id: messageID }
  if (customTag)
    payload.content += customTag
      .split('&')
      .map((el) => `<@${el}>`)
      .join(' ')
  const options = {
    method: 'POST',
    headers: {
      Authorization: `Bot ${process.env.DISCORD_TOKEN}`,
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  }
  const response = await fetch(
    `${DISCORD_URL}/channels/${channelID}/messages`,
    options
  )
  const data = await response.json()
  return data
}
